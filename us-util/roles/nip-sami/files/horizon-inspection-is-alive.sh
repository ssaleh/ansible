#!/bin/bash

IS_ALIVE_INS=$(curl -s -XGET http://localhost:6069/inspection/monitor/health)
while [[ ${IS_ALIVE_INS} != *UP* ]]
do
    sleep 5s
    IS_ALIVE_INS=$(curl -s -XGET http://localhost:6069/inspection/monitor/health)
    echo "Waiting for Horizon-Inspection to boot..."
    echo ${IS_ALIVE_INS}
done
