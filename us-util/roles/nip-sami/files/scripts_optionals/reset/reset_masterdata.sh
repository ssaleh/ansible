#!/bin/bash

if [ $# -eq 0 ]
then
    host=localhost
    folder=horizon
else
    host=$1
    folder=$2
fi

PGPASSWORD=greenzone psql -h $host -p 5432 -d greenzone -U greenzone_admin -f "/home/$folder/scripts_optionals/reset/reset_masterdata.sql"
