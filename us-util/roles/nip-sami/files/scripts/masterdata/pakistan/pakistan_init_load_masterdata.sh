#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

#Master Data Json
sh /home/$folder/scripts/masterdata/generic/scripts/send_closure_reasons_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_notes_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_not_feasible_reasons_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_scans_md_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_cache_config_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_inspection_tool_settings_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_regexp.sh $login $pass $host $folder

sh /home/$folder/scripts/masterdata/generic/scripts/send_code_types_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_eco_sectors_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_geo_areas_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_bizes_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_config_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_users_sample_json.sh $login $pass $host $folder

exit 0