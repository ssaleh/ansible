#!/bin/sh

IS_ALIVE=$(curl -s -XGET http://localhost:6099/sicpatrace/simulator/version)
while [[ ${IS_ALIVE} != *v.* ]]
do
    sleep 5s
    IS_ALIVE=$(curl -s -XGET http://localhost:6099/sicpatrace/simulator/version)
    echo "Waiting for Horizon Sicpatrace Simulator to boot..."
    echo ${IS_ALIVE}
done

curl -v -XGET "http://localhost:6099/sicpatrace/simulator/populate"

exit 0