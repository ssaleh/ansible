#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

token=`sh /home/$folder/scripts/utils/get_token.sh $login $pass $host`

## now we create an example campaign having two bizes
json="{\"name\": \"Hospitals in Bomet, Kenya\", \"description\": \"There should be two business Mulot Dispensary and Longisa County Hospital\", \
\"startDate\": \"31/12/2016\", \"endDate\": \"31/12/2018\", \"sectors\": [\"612\"], \"zones\": [\"1500\"]}"

curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d"$json" "http://$host/services/campaigns/register"

json="{\"name\": \"All 6 bizes in Kenya\", \"description\": \"There should be six bizes here\", \
\"startDate\": \"31/12/2016\", \"endDate\": \"31/12/2018\", \"sectors\": [\"612\",\"457\",\"357\",\"372\"], \"zones\": [\"1500\",\"1457\"]}"

curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d"$json" "http://$host/services/campaigns/register"