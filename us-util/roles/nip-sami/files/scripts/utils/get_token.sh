#!/bin/bash

if [ $# -eq 0 ]
then
    user=admin
    pass=1234
    host=localhost
else
    user=$1
    pass=$2
    host=$3
fi

token_response=`curl -v -X POST -H "Content-Type: application/json" "http://$host/services/login" --data-binary "{\"grant_type\":\"password\",\"username\":\"$user\",\"password\":\"$pass\"}"`

token=`echo $token_response | sed -e "s/{\"access_token\":\"\([^\"]*\)\".*/\1/"`

echo "$token"