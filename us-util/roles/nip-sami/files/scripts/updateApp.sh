#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
yum clean all
service httpd stop
service horizon-bus stop
service horizon-inspection stop
service horizon-masterdata stop
service horizon-security stop

echo "Updating horizon-bus, horizon-inspection-package , horizon-masterdata, horizon-security and nip-webapp-package"

yum update -y horizon-bus-package horizon-inspection-package nip-webapp-package horizon-masterdata-package horizon-security-package

rm /etc/init.d/horizon-bus
chmod 755 /usr/share/horizon-bus/lib/horizon-bus*.jar
ln -s /usr/share/horizon-bus/lib/horizon-bus*.jar /etc/init.d/horizon-bus

rm /etc/init.d/horizon-inspection
chmod 755 /usr/share/horizon-inspection/lib/horizon-inspection*.jar
ln -s /usr/share/horizon-inspection/lib/horizon-inspection*.jar /etc/init.d/horizon-inspection

rm /etc/init.d/horizon-masterdata
chmod 755 /usr/share/horizon-masterdata/lib/horizon-masterdata*.jar
ln -s /usr/share/horizon-masterdata/lib/horizon-masterdata*.jar /etc/init.d/horizon-masterdata

rm /etc/init.d/horizon-security
chmod 755 /usr/share/horizon-security/lib/horizon-security*.jar
ln -s /usr/share/horizon-security/lib/horizon-security*.jar /etc/init.d/horizon-security

service httpd start
service horizon-security start
service horizon-masterdata start
service horizon-inspection start
service horizon-bus start

