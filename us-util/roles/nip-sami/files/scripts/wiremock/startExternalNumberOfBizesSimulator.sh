#!/bin/sh


## starts a service on wiremock to simulate the response from a external system to calculate the number of bizes
curl -X POST --data '{ "request": { "urlPath": "/erp/number-of-bizes", "method": "GET"}, "response": { "status": 200, "body": "312" }}' http://localhost:9999/__admin/mappings/new