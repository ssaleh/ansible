#!/bin/sh

clear
printf " App host? (default: localhost): "
read -r appHost
if [ -z "$appHost" ]
then
   appHost=localhost
fi
printf " BBDD host? (default: localhost): "
read -r bdHost
if [ -z "$bdHost" ]
then
   bdHost=localhost
fi
printf " Admin user? (default: admin): "
read -r login
if [ -z "$login" ]
then
   login=admin
fi
printf  " Password? (default: 1234): "
read -r pass
if [ -z "$pass" ]
then
   pass=1234
fi
printf " Scripts user folder? (default: horizon): "
read -r folder
if [ -z "$folder" ]
then
   folder=horizon
fi
clear
while :
do

echo "  ______              _                     _    _______                   "
echo " (____  \            | |                   | |  (_______)                  "
echo "  ____)  )_____  ____| |  _ _____ ____   __| |   _  _  _ _____ ____  _   _ "
echo " |  __  ((____ |/ ___) |_/ ) ___ |  _ \ / _  |  | ||_|| | ___ |  _ \| | | |"
echo " | |__)  ) ___ ( (___|  _ (| ____| | | ( (_| |  | |   | | ____| | | | |_| |"
echo " |______/\_____|\____)_| \_)_____)_| |_|\____|  |_|   |_|_____)_| |_|____/ "
echo " "
echo "--------------------------------------------------------------------------"
echo " App Host -> $appHost  BD Host -> $bdHost  Admin user: $login "
echo " User home: $folder"
echo " "
echo "  What do you want to do? "
echo "  "
echo "  1. Load all masterdata"
echo "  2. Load Closure Reasons"
echo "  3. Load Issues Category"
echo "  4. Load Notes Category"
echo "  5. Load Not Feasible Reasons"
echo "  6. Load Scans Actions, Categories and Reasons"
echo "  7. Load Mobile Cache Configuration"
echo "  8. Load Mobile Inspection Tool Settings"
echo "  9. Load Regular Expressions"
echo " 10. Load License Issues Category"
echo " 11. Load License Notes Category"
echo " 12. Load Stamps Issues Category"
echo " 13. Load Stamps Notes Category"
echo " 14. Load Code Types"
echo " 15. Load Economic Sectors"
echo " 16. Load Geographical Areas"
echo " 17. Load Bizes Types"
echo " 18. Load Business"
echo " 19. Load Business Profile Configuration"
echo " 20. Load Business Excise Configuration"
echo " 21. Load Users"
echo " 22. Load System Options"
echo " 23. Load BI-Reports views"
echo " ------------------------------------------------"
echo " Pakistan"
echo " ------------------------------------------------"
echo " 30. Load Pakistan all masterdata"
echo " 31. Load Pakistan Issues Category"
echo " 32. Load Pakistan Notes Category"
echo " 33. Load Pakistan Regular Expressions"
echo " 34. Load Pakistan Economic Sectors"
echo " 35. Load Pakistan Geographical Areas"
echo " 36. Load Pakistan Business"
echo " 37. Load Pakistan Users"
echo " ------------------------------------------------"
echo " 98. Reset all masterdata"
echo " 99. Reset all inspections info"
echo "  0. Exit"
echo " "
printf " Select option: "
read -r opt
case $opt in
1) echo " Loading all masterdata";
sh /home/$folder/scripts/masterdata/generic/generic_init_load_masterdata.sh $login $pass $appHost $folder;;
2) echo " Loading Closure Reasons";
sh /home/$folder/scripts/masterdata/generic/scripts/send_closure_reasons_sample_json.sh $login $pass $appHost $folder;;
3) echo " Loading Issues Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_issues_cat_sample_json.sh $login $pass $appHost $folder;;
4) echo " Loading Notes Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_notes_cat_sample_json.sh $login $pass $appHost $folder;;
5) echo " Loading Not Feasible Reasons";
sh /home/$folder/scripts/masterdata/generic/scripts/send_not_feasible_reasons_sample_json.sh $login $pass $appHost $folder;;
6) echo " Loading Scans Actions, Categories and Reasons";
sh /home/$folder/scripts/masterdata/generic/scripts/send_scans_md_json.sh $login $pass $appHost $folder;;
7) echo " Loading Mobile Cache Configuration";
sh /home/$folder/scripts/masterdata/generic/scripts/send_cache_config_sample_json.sh $login $pass $appHost $folder;;
8) echo " Loading Mobile Inspection Tool Settings";
sh /home/$folder/scripts/masterdata/generic/scripts/send_inspection_tool_settings_sample_json.sh $login $pass $appHost $folder;;
9) echo " Loading Regular Expressions";
sh /home/$folder/scripts/masterdata/generic/scripts/send_regexp.sh $login $pass $appHost $folder;;
10) echo " Loading License Issues Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_license_issues_cat_sample_json.sh $login $pass $appHost $folder;;
11) echo " Loading License Notes Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_license_notes_cat_sample_json.sh $login $pass $appHost $folder;;
12) echo " Loading Stamps Issues Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_stamps_issues_cat_sample_json.sh $login $pass $appHost $folder;;
13) echo " Loading Stamps Notes Category";
sh /home/$folder/scripts/masterdata/generic/scripts/send_stamps_notes_cat_sample_json.sh $login $pass $appHost $folder;;
14) echo " Loading Code Types";
sh /home/$folder/scripts/masterdata/generic/scripts/send_code_types_sample_json.sh $login $pass $appHost $folder;;
15) echo " Loading Economic Sectors";
sh /home/$folder/scripts/masterdata/generic/scripts/send_eco_sectors_sample_json.sh $login $pass $appHost $folder;;
16) echo " Loading Geographical Areas";
sh /home/$folder/scripts/masterdata/generic/scripts/send_geo_areas_sample_json.sh $login $pass $appHost $folder;;
17) echo " Loading Bizes Types";
sh /home/$folder/scripts/masterdata/generic/scripts/send_md_bizes_types_json.sh $login $pass $appHost $folder;;
18) echo " Loading Business";
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_sample_json.sh $login $pass $appHost $folder;;
19) echo " Loading Business Profile Configuration";
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_config_sample_json.sh $login $pass $appHost $folder;;
20) echo " Loading Business Excise Configuration";
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_config_excise_json.sh $login $pass $appHost $folder;;
21) echo " Loading Users";
sh /home/$folder/scripts/masterdata/generic/scripts/send_users_sample_json.sh $login $pass $appHost $folder;;
22) echo " Loading System Options";
sh /home/$folder/scripts/masterdata/generic/scripts/send_system_options.sh $login $pass $appHost $folder;;
23) echo " Loading BI-Reports views";
sh /home/$folder/scripts/masterdata/generic/scripts/send_bi_reports.sh $login $pass $appHost $folder;;

30) echo " Loading Pakistan all masterdata";
sh /home/$folder/scripts/masterdata/pakistan/pakistan_init_load_masterdata.sh $login $pass $appHost $folder;;
31) echo " Loading Pakistan Issues Category";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_issues_cat_sample_json.sh $login $pass $appHost $folder;;
32) echo " Loading Pakistan Notes Category";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_notes_cat_sample_json.sh $login $pass $appHost $folder;;
33) echo " Loading Pakistan Regular Expressions";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_regexp.sh $login $pass $appHost $folder;;
34) echo " Loading Pakistan Economic Sectors";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_eco_sectors_sample_json.sh $login $pass $appHost $folder;;
35) echo " Loading Pakistan Geographical Areas";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_geo_areas_sample_json.sh $login $pass $appHost $folder;;
36) echo " Loading Pakistan Business";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_bizes_sample_json.sh $login $pass $appHost $folder;;
37) echo " Loading Pakistan Users";
sh /home/$folder/scripts/masterdata/pakistan/scripts/send_pakistan_users_sample_json.sh $login $pass $appHost $folder;;

98) echo " Deleting all masterdata";
sh /home/$folder/scripts_optionals/reset/reset_masterdata.sh $bdHost $folder;;
99) echo " Deleting all inspection info";
sh /home/$folder/scripts_optionals/reset/reset.sh $bdHost $folder;;
0)echo " "
  echo " Bye (^ - ^) /";
  echo " "
  exit 0;;
 *) printf " Invalid option, press any key to continue...";
    read -r any;
    clear;;
esac
done
