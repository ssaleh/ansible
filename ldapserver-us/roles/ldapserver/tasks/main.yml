---
# tasks file for ldap-server
- name: Install openldap server
  yum: name=openldap-servers state=present
  become: yes
  remote_user: root

- name: Install samba
  yum: name=samba state=present

- name: Install freeradius
  yum: name=freeradius state=present

- name: Install pam module
  yum: name=nss-pam-ldapd state=present

- name: Create the openldap log directory
  file: mode=0755 owner=ldap group=ldap state=directory path=/var/log/openldap

- name: Remove the clashing configuration directory
  file: path=/etc/openldap/slapd.d state=absent

- name: Insert a dummy freeradius config
  file: path=/etc/openldap/schema/freeradius.schema state=touch owner=ldap group=ldap mode=644

- name: Open the port for LDAP auth
  firewalld: service=ldap permanent=yes immediate=true state=enabled

- name: Creating some LDAP directories.
  file: mode=0755 owner=root group=root state=directory path=/etc/openldap/{{ item }} 
  with_items:
   - cacerts
   - certs
   - schema

- name: Creating some more LDAP directories.
  file: mode=0755 owner=ldap group=ldap state=directory path=/etc/openldap/{{ item }}
  with_items:
   - overlays.conf.d
   - schemas.conf.d
   - ldifs.d
   - domains.conf.d

- name: Copying files in place.
  copy: mode=0644 owner=ldap group=ldap src={{ item }}  dest=/etc/openldap/
  with_items:
  - slapd.conf
  - overlays.conf
  - schemas.conf

- name: Copying check_password.conf in place
  copy: mode=0640 owner=root group=ldap src=check_password.conf dest=/etc/openldap/

- name: Copying the acl.conf in place
  template: mode=0644 src=acls.conf.j2 owner=root group=root dest=/etc/openldap/acls.conf

- name: Copying the domains.conf in place
  template: mode=0644 src=domains.conf.j2 owner=root group=root dest=/etc/openldap/domains.conf 

- name: Copying the sldap.conf file in place for {{ project }}
# sami   template: mode=0640 src=dms-project.sldap.conf.j2 owner=ldap group=ldap dest=/etc/openldap/domains.conf.d/dms-{{ project }}-lan-sldap.conf
  template: mode=0640 src=dms-project.sldap.conf.j2 owner=ldap group=ldap dest=/etc/openldap/domains.conf.d/us-dc-lan-sldap.conf

- name: Copying the empty file because why not.
  copy: mode=0644 owner=root group=root src=empty dest=/etc/openldap/domains.conf.d/empty

- name: Copying the ldap.conf in place.
  template: mode=0644 src=ldap.conf.j2 dest=/etc/openldap/ldap.conf owner=root group=root

- name: Set up the LDAP base domain.
# sami  template: mode=0644 src=dms-project-lan_baseDN.ldif.j2 dest=/etc/openldap/ldifs.d/dms-{{ project }}-lan_baseDN.ldif
  template: mode=0644 src=dms-project-lan_baseDN.ldif.j2 dest=/etc/openldap/ldifs.d/us-dc-lan_baseDN.ldif

- name: Prepare the root LDAP directory
#  file: mode=0755 owner=ldap group=ldap state=directory path=/var/lib/ldap/dms
  file: mode=0755 owner=ldap group=ldap state=directory path=/var/lib/ldap/usdc

- name: Create LDAP USDC Folder if it does not exist
  file:
    path: /var/lib/ldap/usdc
    state: directory
    owner: ldap
    group: ldap

- name: Create LDAP USDC DC Folder if it does not exist
  file:
    path: /var/lib/ldap/usdc/dc=us-dc,dc=lan
    state: directory
    owner: ldap
    group: ldap

- name: Create LDAP People Folder if it does not exist
  file:
    path: /var/lib/ldap/usdc/dc=us-dc,dc=lan/ou=people
    state: directory
    owner: ldap
    group: ldap

- name: Copying the main LDIF file
# sami  template: mode=0600 owner=ldap group=ldap src=dc=dms,dc=project,dc=lan.ldif.j2 dest=/var/lib/ldap/dms/dc=dms,dc={{ project }},dc=lan.ldif
  template: mode=0600 owner=ldap group=ldap src=dc=us-dc,dc=lan.ldif.j2 dest=/var/lib/ldap/usdc/dc=us-dc,dc=lan.ldif
# dc=us-dc,dc=lan.ldif.j2

- name: Create the directory where the LDAP lives
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }} 

- name: Copy bunch of LDAP templates in place
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ldapdir}}/{{ item }}
  with_items:
# sam    - ou=membershipmanager.ldif
# sami    - ou=kvm.ldif
   - ou=monitoringgroups.ldif
   - ou=othergroups.ldif
   - ou=people.ldif
   - ou=serviceusers.ldif
   - ou=unixgroups.ldif
   - ou=vpngroups.ldif
   - ou=websql.ldif
   - ou=wingroups.ldif
   - sambaDomainName=dms.ldif

- name: Create some more directories to go with it
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/{{ item }}
  with_items:
   - ou=websql
   - ou=wingroups
   - ou=serviceusers
   - ou=monitoringgroups
   - ou=vpngroups
# sami   - ou=kvm
   - ou=othergroups
   - ou=unixgroups
   - ou=people

# sami - name: Copy the KVM group
#   template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=kvm/{{ item }}
# sami -   template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest=/var/lib/
# sami   with_items:
# sami   - cn=kvmadministrators.ldif
# sami   - cn=kvm.ldif

- name: Copy the monitoring group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=monitoringgroups/{{ item }}
  with_items:
   - cn=admin.ldif
   - cn=operator.ldif
   - cn=user.ldif

- name: Copy the othergroup group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=othergroups/{{ item }}
  with_items:
   - cn=bluecoat-admins.ldif  
   - cn=securitycenter-users.ldif  
   - cn=vmwareadministrators.ldif
   - cn=storage-admin.ldif
   - cn=websql-manager.ldif

- name: Copy the people group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest=/var/lib/ldap/usdc/dc=us-dc,dc=lan/ou=people/{{ item }}
  with_items:
   - uid=jraab.ldif  
   - uid=aagarwal.ldif
   - uid=adepagnier.ldif
   - uid=afredriksen.ldif
   - uid=ahowell.ldif
   - uid=albertodls.ldif
   - uid=anewman.ldif
   - uid=aporter.ldif
   - uid=asharif.ldif
   - uid=aspelman.ldif
   - uid=avaldaliso.ldif
   - uid=ayasrebi.ldif
   - uid=bahmed.ldif
   - uid=bclark.ldif
   - uid=bmahon.ldif
   - uid=bpence.ldif
   - uid=br-enagamine.ldif
   - uid=c2pulse.ldif
   - uid=cbaptiste.ldif
   - uid=cbarreiro.ldif
   - uid=cburford.ldif
   - uid=ccunningham.ldif
   - uid=cmauck.ldif
   - uid=cramsay.ldif
   - uid=cramsey.ldif
   - uid=ctrigo.ldif
   - uid=dbarnes.ldif
   - uid=dbtest.ldif
   - uid=dking.ldif
   - uid=dmetheny.ldif
   - uid=doliveira.ldif
   - uid=dougbfresh.ldif
   - uid=dpandey.ldif
   - uid=dwells.ldif
   - uid=egezahagne.ldif
   - uid=fbaptista.ldif
   - uid=fernet.ldif
   - uid=ftabarra.ldif
   - uid=ftabbara.ldif
   - uid=ggouveia.ldif
   - uid=ghazzaoui.ldif
   - uid=goopingai.ldif
   - uid=gpusapati.ldif
   - uid=gpuspati.ldif
   - uid=hvemula.ldif
   - uid=ikumarasamy.ldif
   - uid=inspector.ldif
   - uid=jalencar.ldif
   - uid=jchallapalli.ldif
   - uid=jcox.ldif
   - uid=jenkins.ldif
   - uid=jfisher.ldif
   - uid=jfrancisco.ldif
   - uid=jfrazao.ldif
   - uid=jgottschling.ldif
   - uid=jleon.ldif
   - uid=jmanalel.ldif
   - uid=jnakhle.ldif
   - uid=jnunes.ldif
   - uid=jpgomez.ldif
   - uid=jraab.ldif
   - uid=jrodrigues.ldif
   - uid=jsmith.ldif
   - uid=jtufano.ldif
   - uid=kchaitanyasanagapally.ldif
   - uid=kfeldman.ldif
   - uid=kgunda.ldif
   - uid=kmowry.ldif
   - uid=knas.ldif
   - uid=lannamareddy.ldif
   - uid=lbianchini.ldif
   - uid=lignatti.ldif
   - uid=lrodrigo.ldif
   - uid=malvord.ldif
   - uid=mgupta.ldif
   - uid=mlyzhenko.ldif
   - uid=mmorollon.ldif
   - uid=mmouhtaj.ldif
   - uid=mseguin.ldif
   - uid=my-goopingai.ldif
   - uid=nnguyen.ldif
   - uid=nvangara.ldif
   - uid=pburgisser.ldif
   - uid=pchandratreya.ldif
   - uid=pfigueiredo.ldif
   - uid=phorrigan.ldif
   - uid=pimmadi.ldif
   - uid=rabbas.ldif
   - uid=rchoudhury.ldif
   - uid=rdeshireddy.ldif
   - uid=rmenon.ldif
   - uid=rrajireddy.ldif
   - uid=rreedjr.ldif
   - uid=rrinco.ldif
   - uid=rvmokate.ldif
   - uid=salvarez.ldif
   - uid=scasal.ldif
   - uid=schebrolu.ldif
   - uid=schebulu.ldif
   - uid=sdebroy.ldif
   - uid=searl.ldif
   - uid=sganna.ldif
   - uid=sghazzaoui.ldif
   - uid=skallem.ldif
   - uid=snadipalli.ldif
   - uid=snayyar.ldif
   - uid=snekkanti.ldif
   - uid=ssaleh.ldif
   - uid=svc_bluecoat.ldif
#    - uid=svc_checkmk.ldif
   - uid=svc_idrac.ldif
# sami   - uid=svc_kvm.ldif
   - uid=svc_ldap.ldif
   - uid=svc_maggi_splunk.ldif
   - uid=svc_paas_pp.ldif
#   - uid=svc_vmwareh5.ldif
#   - uid=svc_vmware.ldif
   - uid=ttest.ldif
   - uid=us-bmahon.ldif
   - uid=us-cmahon.ldif
   - uid=us-dacuff.ldif
   - uid=us-jconnors.ldif
   - uid=us-jjohnson.ldif
   - uid=us-jsharps.ldif
   - uid=us-jskinner.ldif
   - uid=us-ljeffries.ldif
   - uid=us-lmiller.ldif
   - uid=us-mpiccolo.ldif
   - uid=us-rmattson.ldif
   - uid=us-skirmani.ldif
   - uid=vkumar.ldif
   - uid=vnaik.ldif
   - uid=vparre.ldif
   - uid=vreddy.ldif
   - uid=wlasso.ldif
   - uid=yzhao.ldif

- name: Copy the serviceusers group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=serviceusers/{{ item }}
  with_items:
#   - uid=svc_checkmk.ldif 
#   - uid=svc_kvm.ldif   
#   - uid=svc_maggi_splunk.ldif  
#   - uid=svc_vmwareh5.ldif
   - uid=svc_bluecoat.ldif  
#   - uid=svc_nessus.ldif  
#   - uid=svc_idrac.ldif    
#   - uid=svc_ldap.ldif  
#   - uid=svc_paas_pp.ldif       
#   - uid=svc_vmware.ldif
#   - uid=svc_vpn.ldif
#   - uid=svc_vr.ldif

- name: Create the subdirectories in the serviceusers group
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/ou=serviceusers/{{item}}
  with_items:
   - uid=svc_bluecoat

- name: Copy more serviceusers files inside ... we're nearing the end.
  template: mode=0600 owner=ldap group=ldap src={{ item }}.ldif.j2 dest={{ ldapdir }}/ou=serviceusers/{{ item }}/{{ item }}.ldif
  with_items:
  - uid=svc_bluecoat

#- name: Copy the unixgroups.
#  template: mode=0600 owner=ldap group=ldap src=cn=ch-dc-{{ item }}.j2 dest={{ ldapdir }}/ou=unixgroups/cn={{ project }}-{{ item }}
#  with_items:
#  - app-admin.ldif
#  - db-admin.ldif
#  - net-admin.ldif
#  - users.ldif
#  - app-support.ldif
#  - local-it.ldif
#  - site-support.ldif
#  - wheel.ldif  

#- name: Copy the VPN groups
#  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=vpngroups/{{ item }}
#  with_items:
#  - cn=vpn-app.ldif
#  - cn=vpn-basic.ldif
#  - cn=vpn-db.ldif
#  - cn=vpn-netadmin.ldif
#  - cn=vpn-superuser.ldif

- name: Create the websql subdirectories
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/ou=websql/{{ item }}
  with_items:
  - ou=computergroups
  - ou=computers
  - ou=roles

- name: Copy the main files inside
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=websql/{{ item }}
  with_items:
  - ou=computergroups.ldif
  - ou=computers.ldif
  - ou=roles.ldif

- name: Copy the all sql entry
  template: mode=0600 owner=ldap group=ldap src=cn=all.ldif.j2 dest={{ ldapdir }}/ou=websql/ou=computergroups/cn=all.ldif

- name: Copy the websql manager entry
  template: mode=0600 owner=ldap group=ldap src=cn=websql-manager.ldif.j2 dest={{ ldapdir }}/ou=websql/ou=roles/cn=websql-manager.ldif

- name: slapd-arg permissions
  file: 
    path: /var/run/openldap/slapd.arg
    owner: ldap
    group: ldap
    state: touch

- name: Start and enable the ldap service
  service: 
     name=slapd 
     enabled=yes 
     state=started

- name: Waiting for LDAP server to start ...
  wait_for: port=389 state=present delay=5 

- name: Configure the local LDAP authentication
  command: authconfig --enableldap --enableldapauth --ldapserver=ldap://127.0.0.1 --ldapbasedn=dc=us-dc,dc=lan --enablemkhomedir  --update

- name: Retrieve the timestamp
  command: date +%s
  register: timestamp
#- name: (gulp) Register the DNS entry for ldap
#  lineinfile: insertafter="IN      A" line="ldap       IN      A                       {{ ldap_ipaddress }}" path=/var/named/data/{{ project }}.lan.zone
#  delegate_to: "{{ groups.bindmaster[0] }}"
#- name: Update the bind serial
#  lineinfile: regexp="^.*; Serial$" path=/var/named/data/{{ project }}.lan.zone line="        {{ timestamp.stdout }}         ; Serial"
#  notify: reload_bind
 
