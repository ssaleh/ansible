#!/usr/bin/env python
##############################################################################
# This script was made to be able to use a pre-generated inventory.json in   #
# jenkins hosts where no Openstack tools are available.                      #
# If you prefer a yaml or dynamic inventory, change settings in ansible.cfg  #
##############################################################################
import os

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(os.path.join(__location__, "inventory.json")) as f:
    print f.read()
