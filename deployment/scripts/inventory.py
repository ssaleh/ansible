#!/usr/bin/env python

import os_client_config
import os, sys, json
import ipaddress
import yaml

dest_path = "inventory.json"
OS_METADATA_KEY = {
	'host_groups': 'ansible_host_groups',
	'host_vars': 'ansible_host_vars'
}

def main(args):
	# set up the external network name
	if "OS_NETWORK_NAME" in os.environ:
		OS_NETWORK_NAME = os.environ["OS_NETWORK_NAME"]
	else:
		if "OS_PROJECT_NAME" in os.environ:
			OS_NETWORK_NAME = os.environ["OS_PROJECT_NAME"]+"-net"
		else:
			print "ERROR: OpenStack access variables must be loaded"
			sys.exit(os.EX_CONFIG)
	#Initialize openstack client
	session = os_client_config.session_client('compute')
	#Get al the server for the OS credentials
	response = session.get('/servers')
	server_list = response.json()['servers']

	inventory = {}
	inventory['_meta'] = { 'hostvars': { "localhost": { "ansible_ssh_host": "localhost" } } }

	for server in server_list:
		#Request the detailed info for each server
		each = session.get('/servers/'+server['id'])
		each_server = each.json()['server']

		floatingIp = getFloatingIpFromServerForNetwork(each_server, OS_NETWORK_NAME)
		if floatingIp:
			addServerToHostGroup(each_server['name'], floatingIp, inventory)
			for group in getAnsibleHostGroupsFromServer(each_server):
				addServerToHostGroup(group, floatingIp, inventory)
			host_vars = getAnsibleHostVarsFromServer(each_server)
			if host_vars:
				addServerHostVarsToHostVars(host_vars, floatingIp, inventory)

	dumpInventoryAsJson(inventory)

def getAnsibleHostGroupsFromServer(server):
	try:
		metadata = server['metadata'][OS_METADATA_KEY['host_groups']]
		return metadata.split(',')
	except Exception:
		return []

def getAnsibleHostVarsFromServer(server):
	try:
		metadata = server['metadata'][OS_METADATA_KEY['host_vars']]
		host_vars = {}
		for kv in metadata.split(';'):
			key, values = kv.split('->')
			values = values.split(',')
			host_vars[key] = values
		return host_vars
	except Exception:
		return None

def getFloatingIpFromServerForNetwork(server, network):
	for addr in server['addresses'][network]:
		if addr['OS-EXT-IPS:type'] == 'floating':
			return addr['addr']
	return None

def addServerToHostGroup(group, floatingIp, inventory):
	host_group = inventory.get(group, {})
	hosts = host_group.get('hosts', [])
	hosts.append(floatingIp)
	host_group['hosts'] = hosts
	inventory[group] = host_group

def addServerHostVarsToHostVars(host_vars, floatingIp, inventory):
	inventory_host_vars = inventory['_meta']['hostvars'].get(floatingIp, {})
	inventory_host_vars.update(host_vars)
	inventory['_meta']['hostvars'][floatingIp] = inventory_host_vars

def dumpInventoryAsJson(inventory):
	with open(dest_path,'w') as file:
		file.write(json.dumps(inventory, indent=4,sort_keys=True))
	print(json.dumps(inventory, indent=4,sort_keys=True))


if __name__ == "__main__":
	main(sys.argv)
