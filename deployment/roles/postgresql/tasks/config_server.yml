---
- name: Configure global options
  template:
    src: postgresql.conf.j2
    dest: "/var/lib/pgsql/{{ provision.postgres.version }}/data/postgresql.conf"
    owner: postgres
    group: postgres
    mode: 0600
  notify: restart postgresql

- name: Configure hosr based authentication
  copy: src=pg_hba.conf dest=/var/lib/pgsql/{{ provision.postgres.version }}/data/pg_hba.conf owner=postgres group=postgres mode=0600
  notify: restart postgresql

- name: Ensure PostgreSQL unix socket dirs exist.
  file:
    path: /var/run/postgresql
    state: directory
    owner: "postgres"
    group: "postgres"
    mode: 02775

- name: Ensure PostgreSQL databases are present.
  postgresql_db:
    name: "{{ provision.postgres.db_name }}"
  become: yes
  become_user: postgres

- name: Ensure PostgreSQL users are present.
  postgresql_user:
    name: "{{ item }}"
    password: "{{ provision.postgres.db_pass }}"
    state: present
    no_password_changes: yes
  with_items:
    - "{{ provision.postgres.db_user }}"
    - "{{ provision.postgres.db_role }}"
  become: yes
  become_user: postgres

- name: Grant user privileges on database
  postgresql_privs:
    database: "{{ provision.postgres.db_name }}"
    roles: "{{ provision.postgres.db_role }}"
    privs: "{{ provision.postgres.db_privilege }}"
    type: database
  become: yes
  become_user: postgres

- name: Grant admin user privileges on database
  postgresql_privs:
    database: "{{ provision.postgres.db_name }}"
    roles: "{{ provision.postgres.db_user }}"
    privs: ALL
    type: database
  become: yes
  become_user: postgres

- name: Create postgres pgpass
  lineinfile:
    path: /var/lib/pgsql/.pgpass
    line: "{{provision.postgres.db_host}}:{{provision.postgres.db_port}}:{{provision.postgres.db_name}}:{{provision.postgres.db_user}}:{{provision.postgres.db_pass}}"
    create: yes
    owner: "postgres"
    group: "postgres"
    mode: 0600

- name: create some needed directories
  file:
    path: /var/lib/pgsql/bi_scripts
    state: directory
    owner: "postgres"
    group: "postgres"

- name: create some needed directories
  file:
    path: /home/backups
    state: directory
    mode: 0777

- name: copy sql to refresh snapshots
  copy: src=refresh_materialized_views.sql dest=/var/lib/pgsql/bi_scripts/refresh_materialized_views.sql owner=postgres group=postgres mode=0700

- name: cron to refresh materialized views
  cron:
    name: "refresh materialized views"
    job: "psql --host '{{provision.postgres.db_host}}' --port '{{provision.postgres.db_port}}' --username '{{provision.postgres.db_user}}' --dbname '{{provision.postgres.db_name}}' -f /var/lib/pgsql/bi_scripts/refresh_materialized_views.sql"
    user: "postgres"
    hour: "19"
    minute: "00"

- name: copy backup script
  copy: src=backup_psql.sh dest=/home/backups/backup_psql.sh owner=root group=root mode=0700

- name: create backup cron
  cron:
    name: "database backup"
    job: "/home/backups/backup_psql.sh"
    user: root
    hour: 2
    minute: 30
