#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

token=`sh /home/$folder/scripts/utils/get_token.sh $login $pass $host`

curl -v -XPOST -H "Content-type:application/json" -H "Authorization : Bearer $token" "http://$host/services/visit" -d@/home/$folder/scripts/samples/data/visits.json
curl -v -XPOST -H "Content-type:application/json" -H "Authorization : Bearer $token" "http://$host/services/visit" -d@/home/$folder/scripts/samples/data/visits2.json
curl -v -XPOST -H "Content-type:application/json" -H "Authorization : Bearer $token" "http://$host/services/visit" -d@/home/$folder/scripts/samples/data/visits3.json
curl -v -XPOST -H "Content-type:application/json" -H "Authorization : Bearer $token" "http://$host/services/visit" -d@/home/$folder/scripts/samples/data/visits4.json
curl -v -XPOST -H "Content-type:application/json" -H "Authorization : Bearer $token" "http://$host/services/visit" -d@/home/$folder/scripts/samples/data/visits5.json

