#!/bin/bash

CURRENT_DATE=$( date +%F )
BACKUP_DIR=/home/backups

slapcat -v -b "cn=Manager,dc=nip,dc=sicpa-greenzone,dc=com" -l $BACKUP_DIR/ldap_$CURRENT_DATE.diff 2>&1 >> /var/log/backup_ldap.log
