REFRESH MATERIALIZED VIEW bi.vwm_business;
REFRESH MATERIALIZED VIEW bi.vwm_campaign_geoareas;
REFRESH MATERIALIZED VIEW bi.vwm_campaign_inspectors;
REFRESH MATERIALIZED VIEW bi.vwm_campaign_sectors;
REFRESH MATERIALIZED VIEW bi.vwm_campaigns;
REFRESH MATERIALIZED VIEW bi.vwm_geoareas;
REFRESH MATERIALIZED VIEW bi.vwm_inspections;
REFRESH MATERIALIZED VIEW bi.vwm_inspectors;
REFRESH MATERIALIZED VIEW bi.vwm_reasons;
REFRESH MATERIALIZED VIEW bi.vwm_sectors;
REFRESH MATERIALIZED VIEW bi.vwm_supervisors;
REFRESH MATERIALIZED VIEW bi.vwm_visits;
REFRESH MATERIALIZED VIEW bi.vwm_product_types;
REFRESH MATERIALIZED VIEW bi.vwm_visits_detail;
REFRESH MATERIALIZED VIEW bi.vwm_visits_issues;
REFRESH MATERIALIZED VIEW bi.vwm_visits_scans;
REFRESH MATERIALIZED VIEW bi.vwm_system_options;

SELECT bi.business_split_by_areas_and_sectors();

REFRESH MATERIALIZED VIEW bi.vwm_business;
REFRESH MATERIALIZED VIEW bi.vwm_all_inspection;
