#!/bin/bash

CURRENT_DATE=$( date +%F )
BACKUP_DIR=/home/backups
touch /var/log/backup_psql.log
chown postgres:postgres /var/log/backup_psql.log

# running pg_dump as user postgres
su -c "pg_dump greenzone > $BACKUP_DIR/pg_$CURRENT_DATE.diff 2>> /var/log/backup_psql.log" postgres
