#!/bin/sh

if [ $# -eq 0 ]
then
    user_folder=horizon
else
    user_folder=$1
fi

sh /home/$user_folder/scripts/wiremock/start_wiremock.sh
sleep 15s
sh /home/$user_folder/scripts/wiremock/startKehoAuthSimulator.sh
sh /home/$user_folder/scripts/wiremock/startExternalNumberOfBizesSimulator.sh

exit 0
