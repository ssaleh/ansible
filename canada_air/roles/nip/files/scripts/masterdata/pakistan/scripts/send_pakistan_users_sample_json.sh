#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

token=`sh /home/$folder/scripts/utils/get_token.sh $login $pass $host`

curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_1.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_2.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_3.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_4.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_5.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_6.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_7.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_8.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_9.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_pakistan_10.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/pakistan/data/inspector_manager_pakistan_1.json "http://$host/services/user-management/create-user"
