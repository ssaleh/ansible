#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

token=`sh /home/$folder/scripts/utils/get_token.sh $login $pass $host`

curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_1.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_2.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_3.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_b.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_manager_1.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/inspector_manager_2.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/administrator_1.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/manager_1.json "http://$host/services/user-management/create-user"
curl -v -XPOST -H "Content-type:application/json" -H "Authorization: Bearer $token" -d@/home/$folder/scripts/masterdata/generic/data/manager_2.json "http://$host/services/user-management/create-user"
