#!/bin/sh

if [ $# -eq 0 ]
then
    login=admin
    pass=1234
    host=localhost
    folder=horizon
else
    login=$1
    pass=$2
    host=$3
    folder=$4
fi

IS_ALIVE_BUS=$(curl -s -XGET http://localhost:6067/services/monitor/health)
while [[ ${IS_ALIVE_BUS} != *UP* ]]
do
    sleep 5s
    IS_ALIVE_BUS=$(curl -s -XGET http://localhost:6067/services/monitor/health)
    echo "Waiting for Horizon-BUS to boot..."
    echo ${IS_ALIVE_BUS}
done
echo "Running " ${IS_ALIVE_BUS}

IS_ALIVE_MD=$(curl -s -XGET http://localhost:6070/md/monitor/health)
while [[ ${IS_ALIVE_MD} != *UP* ]]
do
    sleep 5s
    IS_ALIVE_MD=$(curl -s -XGET http://localhost:6070/md/monitor/health)
    echo "Waiting for Horizon-Masterdata to boot..."
    echo ${IS_ALIVE_MD}
done
echo "Running " ${IS_ALIVE_MD}

#Master Data Json
sh /home/$folder/scripts/masterdata/generic/scripts/send_closure_reasons_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_notes_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_not_feasible_reasons_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_scans_md_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_cache_config_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_inspection_tool_settings_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_regexp.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_license_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_license_notes_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_stamps_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_stamps_notes_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_md_bizes_types_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_stockbooksales_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_stockbooksales_notes_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_production_issues_cat_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_production_notes_cat_sample_json.sh $login $pass $host $folder

sh /home/$folder/scripts/masterdata/generic/scripts/send_code_types_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_eco_sectors_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_geo_areas_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_config_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_bizes_config_excise_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_users_sample_json.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_md_logo_mobile.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_system_options.sh $login $pass $host $folder
sh /home/$folder/scripts/masterdata/generic/scripts/send_bi_reports.sh $login $pass $host $folder

exit 0