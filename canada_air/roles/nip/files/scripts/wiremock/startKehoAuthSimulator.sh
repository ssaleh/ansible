#!/bin/sh

## starts a service on wiremock to simulate the reponse from keho sicpatrace
#admin
curl -X POST --data '{ "request": { "url": "/st/keho/auth", "method": "GET", "headers": {"Authorization": {"equalTo": "Basic aG9yYWRtaW46SE9SYWRtaW4wMA=="}} }, "response": { "status": 200, "body": "{\"permissions\":[{\"permission\": \"security.permission.value.horizon_admin\"}]}" }}' http://localhost:9999/__admin/mappings/new

#supervisor
curl -X POST --data '{ "request": { "url": "/st/keho/auth", "method": "GET", "headers": {"Authorization": {"equalTo": "Basic aG9yc3VwZXI6SE9Sc3VwZXIwMA=="}} }, "response": { "status": 200, "body": "{\"permissions\":[{\"permission\": \"security.permission.value.horizon_supervisor\"}]}" }}' http://localhost:9999/__admin/mappings/new

#inspector
curl -X POST --data '{ "request": { "url": "/st/keho/auth", "method": "GET", "headers": {"Authorization": {"equalTo": "Basic aG9yaW5zcGVjOkhPUmluczAw"}} }, "response": { "status": 200, "body": "{\"permissions\":[{\"permission\": \"security.permission.value.horizon_inspector\"}]}" }}' http://localhost:9999/__admin/mappings/new
