#!/bin/bash

IS_ALIVE_MD=$(curl -s -XGET http://localhost:6070/md/monitor/health)
while [[ ${IS_ALIVE_MD} != *UP* ]]
do
    sleep 5s
    IS_ALIVE_MD=$(curl -s -XGET http://localhost:6070/md/monitor/health)
    echo "Waiting for Horizon-Masterdata to boot..."
    echo ${IS_ALIVE_MD}
done
