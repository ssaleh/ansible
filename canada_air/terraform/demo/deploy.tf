# Security group definition
resource "openstack_compute_secgroup_v2" "service" {
  name        = "service_${var.env}"
  description = "enable all ingress traffic. Security provided by OS firewall"

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "udp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }
}

#Create App server
resource "openstack_compute_instance_v2" "app_server" {
  name            = "${lookup(var.app_vm_name, var.env)}"
  image_name      = "Centos7"
  flavor_id       = "${lookup(var.flavor, var.env)}"
  key_pair        = "deploy"
  security_groups = ["default", "service_${var.env}"]
  metadata {
    ansible_host_groups  = "horizon_app,app,horizon_app_${var.env},app_${var.env},${var.env},apache,nip"
  }
}

resource "openstack_networking_floatingip_v2" "app_server_float" {
  pool = "net-iaas-external-dev"
}

resource "openstack_compute_floatingip_associate_v2" "app_server_float_assoc" {
  floating_ip = "${openstack_networking_floatingip_v2.app_server_float.address}"
  instance_id = "${openstack_compute_instance_v2.app_server.id}"
}

#Create DB server
resource "openstack_compute_instance_v2" "db_server" {
  name            = "${lookup(var.db_vm_name, var.env)}"
  image_name      = "Centos7"
  flavor_id       = "${lookup(var.flavor, var.env)}"
  key_pair        = "deploy"
  security_groups = ["default", "service"]
  metadata {
    ansible_host_groups  = "horizon_db,db,horizon_db_${var.env},db_${var.env},${var.env},postgres,ldap"
  }
}

resource "openstack_networking_floatingip_v2" "db_server_float" {
  pool = "net-iaas-external-dev"
}

resource "openstack_compute_floatingip_associate_v2" "db_server_float_assoc" {
  floating_ip = "${openstack_networking_floatingip_v2.db_server_float.address}"
  instance_id = "${openstack_compute_instance_v2.db_server.id}"
}
