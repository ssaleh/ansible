variable "env" {
  description = "env:demo, prepro or sqa"
  default = "demo"
}

variable "flavor" {
  description = "ID of the flavor for the VM"
  type = "map"
}

variable "app_vm_name" {
  description = "Name of the app VM"
  type = "map"
}

variable "db_vm_name" {
  description = "Name of the database VM"
  type = "map"
}
