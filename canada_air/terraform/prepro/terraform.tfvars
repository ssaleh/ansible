flavor = {
  demo = 2 # m1.medium |  4096 |   30 |     2
  prepro = 2 # m1.medium |  4096 |   30 |     2
  sqa = 2 # m1.medium |  4096 |   30 |     2
}

app_vm_name = {
  demo = "horizon-app.demo"
  prepro = "horizon-app.prepro"
  sqa = "horizon-app.sqa"
}

db_vm_name = {
  demo = "horizon-db.demo"
  prepro = "horizon-db.prepro"
  sqa = "horizon-db.sqa"
}
