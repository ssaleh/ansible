---
- name: Install VMWare Tools
  # hosts: ap01-webapp.dms01.tt054.lan
  hosts: ap01-int.dms01.tt008.lan
  gather_facts: no
  become: true

  tasks:
    - synchronize:
        src: VMwareTools-9.10.5-2981885.tar.gz
        dest: /home/ssaleh
