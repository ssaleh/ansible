---
# tasks file for ldap-server
- name: Install openldap server
  yum: name=openldap-servers state=present
- name: Install samba
  yum: name=samba state=present
- name: Install freeradius
  yum: name=freeradius state=present
- name: Install pam module
  yum: name=nss-pam-ldapd state=present
- name: Create the openldap log directory
  file: mode=0755 owner=ldap group=ldap state=directory path=/var/log/openldap
- name: Remove the clashing configuration directory
  file: path=/etc/openldap/slapd.d state=absent
- name: Insert a dummy freeradius config
  file: path=/etc/openldap/schema/freeradius.schema state=touch owner=ldap group=ldap mode=644
#- name: Open the port for LDAP auth
#  firewalld: service=ldap permanent=yes immediate=true state=enabled
- name: Creating some LDAP directories.
  file: mode=0755 owner=root group=root state=directory path=/etc/openldap/{{ item }} 
  with_items:
   - cacerts
   - certs
   - schema
- name: Creating some more LDAP directories.
  file: mode=0755 owner=ldap group=ldap state=directory path=/etc/openldap/{{ item }}
  with_items:
   - overlays.conf.d
   - schemas.conf.d
   - ldifs.d
   - domains.conf.d
- name: Copying files in place.
  copy: mode=0644 owner=ldap group=ldap src={{ item }}  dest=/etc/openldap/
  with_items:
  - slapd.conf
  - overlays.conf
  - schemas.conf
- name: Copying check_password.conf in place
  copy: mode=0640 owner=root group=ldap src=check_password.conf dest=/etc/openldap/
- name: Copying the acl.conf in place
  template: mode=0644 src=acls.conf.j2 owner=root group=root dest=/etc/openldap/acls.conf
- name: Copying the domains.conf in place
  template: mode=0644 src=domains.conf.j2 owner=root group=root dest=/etc/openldap/domains.conf 
- name: Copying the sldap.conf file in place for {{ project }}
  template: mode=0640 src=dms-project.sldap.conf.j2 owner=ldap group=ldap dest=/etc/openldap/domains.conf.d/dms-{{ project }}-lan-sldap.conf

- name: Copying the empty file because why not.
  copy: mode=0644 owner=root group=root src=empty dest=/etc/openldap/domains.conf.d/empty

- name: Copying the ldap.conf in place.
  template: mode=0644 src=ldap.conf.j2 dest=/etc/openldap/ldap.conf owner=root group=root

- name: Set up the LDAP base domain.
  template: mode=0644 src=dms-project-lan_baseDN.ldif.j2 dest=/etc/openldap/ldifs.d/dms-{{ project }}-lan_baseDN.ldif

- name: Prepare the root LDAP directory
  file: mode=0755 owner=ldap group=ldap state=directory path=/var/lib/ldap/dms


- name: Copying the main LDIF file
  template: mode=0600 owner=ldap group=ldap src=dc=dms,dc=project,dc=lan.ldif.j2 dest=/var/lib/ldap/dms/dc=dms,dc={{ project }},dc=lan.ldif

- name: Create the directory where the LDAP lives
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }} 

- name: Copy bunch of LDAP templates in place
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ldapdir}}/{{ item }}
  with_items:
   - cn=membershipmanager.ldif
   - ou=kvm.ldif
   - ou=monitoringgroups.ldif
   - ou=othergroups.ldif
   - ou=people.ldif
   - ou=serviceusers.ldif
   - ou=unixgroups.ldif
   - ou=vpngroups.ldif
   - ou=websql.ldif
   - ou=wingroups.ldif
   - sambaDomainName=dms.ldif
- name: Create some more directories to go with it
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/{{ item }}
  with_items:
   - ou=websql
   - ou=wingroups
   - ou=serviceusers
   - ou=monitoringgroups
   - ou=vpngroups
   - ou=kvm
   - ou=othergroups
   - ou=unixgroups
   - ou=people
- name: Copy the KVM group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=kvm/{{ item }}
  with_items:
   - cn=kvmadministrators.ldif
   - cn=kvm.ldif
- name: Copy the monitoring group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=monitoringgroups/{{ item }}
  with_items:
   - cn=admin.ldif
   - cn=operator.ldif
   - cn=user.ldif
- name: Copy the othergroup group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=othergroups/{{ item }}
  with_items:
   - cn=bluecoat-admins.ldif  
   - cn=securitycenter-users.ldif  
   - cn=vmwareadministrators.ldif
   - cn=pki-admins.ldif       
   - cn=storage-admin.ldif
   - cn=websql-manager.ldif
- name: Copy the people group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=people/{{ item }}
  with_items:
   - uid=jraab.ldif  
   - uid=lndabaneze.ldif 
- name: Copy the serviceusers group
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=serviceusers/{{ item }}
  with_items:
   - uid=svc_checkmk.ldif 
   - uid=svc_kvm.ldif   
   - uid=svc_maggi_splunk.ldif  
   - uid=svc_vmwareh5.ldif
   - uid=svc_bluecoat.ldif  
   - uid=svc_idrac.ldif    
   - uid=svc_ldap.ldif  
   - uid=svc_paas_pp.ldif       
   - uid=svc_pki.ldif  
   - uid=svc_vmware.ldif
- name: Create the subdirectories in the serviceusers group
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/ou=serviceusers/{{item}}
  with_items:
   - uid=svc_bluecoat
   - uid=svc_pki
- name: Copy more serviceusers files inside ... we're nearing the end.
  template: mode=0600 owner=ldap group=ldap src={{ item }}.ldif.j2 dest={{ ldapdir }}/ou=serviceusers/{{ item }}/{{ item }}.ldif
  with_items:
  - uid=svc_pki
  - uid=svc_bluecoat
- name: Copy the unixgroups.
  template: mode=0600 owner=ldap group=ldap src=cn=ch-dc-{{ item }}.j2 dest={{ ldapdir }}/ou=unixgroups/cn={{ project }}-{{ item }}
  with_items:
  - app-admin.ldif
  - db-admin.ldif
  - net-admin.ldif
  - users.ldif
  - app-support.ldif
  - local-it.ldif
  - site-support.ldif
  - wheel.ldif  
- name: Copy the VPN groups
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=vpngroups/{{ item }}
  with_items:
  - cn=vpn-app.ldif
  - cn=vpn-basic.ldif
  - cn=vpn-db.ldif
  - cn=vpn-netadmin.ldif
  - cn=vpn-superuser.ldif
- name: Create the websql subdirectories
  file: mode=0750 owner=ldap group=ldap state=directory path={{ ldapdir }}/ou=websql/{{ item }}
  with_items:
  - ou=computergroups
  - ou=computers
  - ou=roles
- name: Copy the main files inside
  template: mode=0600 owner=ldap group=ldap src={{ item }}.j2 dest={{ ldapdir }}/ou=websql/{{ item }}
  with_items:
  - ou=computergroups.ldif
  - ou=computers.ldif
  - ou=roles.ldif
- name: Copy the all sql entry
  template: mode=0600 owner=ldap group=ldap src=cn=all.ldif.j2 dest={{ ldapdir }}/ou=websql/ou=computergroups/cn=all.ldif
- name: Copy the websql manager entry
  template: mode=0600 owner=ldap group=ldap src=cn=websql-manager.ldif.j2 dest={{ ldapdir }}/ou=websql/ou=roles/cn=websql-manager.ldif

- name: Start and enable the ldap service
  service: name=slapd enabled=yes state=started
- name: Waiting for LDAP server to start ...
  wait_for: port=389 state=present delay=5 
- name: Configure the local LDAP authentication
  command: authconfig --enableldap --enableldapauth --ldapserver=ldap://127.0.0.1 --ldapbasedn=dc=dms,dc={{ project }},dc=lan --enablemkhomedir  --update

- name: Retrieve the timestamp
  command: date +%s
  register: timestamp
- name: (gulp) Register the DNS entry for ldap
  lineinfile: insertafter="IN      A" line="ldap       IN      A                       {{ ldap_ipaddress }}" path=/var/named/data/{{ project }}.lan.zone
  delegate_to: "{{ groups.bindmaster[0] }}"
- name: Update the bind serial
  lineinfile: regexp="^.*; Serial$" path=/var/named/data/{{ project }}.lan.zone line="        {{ timestamp.stdout }}         ; Serial"
  notify: reload_bind
 
